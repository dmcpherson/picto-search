# picto-search (working title)

## Members
* [Devin McPherson](#dm) - [mcpherdp@mail.uc.edu](mailto:mcpherdp@mail.uc.edu)
* [Gerhard Tetzner](#gt) - [tetznegt@mail.uc.edu](mailto:mcpherdp@mail.uc.edu)
* [Matt Flowers](#mf) - [flowermm@mail.uc.edu](mailto:flowermm@mail.uc.edu)
* [Kyle Romard](#kr) - [romardkt@mail.uc.edu](mailto:romardkt@mail.uc.edu)

## Faculty Advisor - Dr. Nan Niu

## Project Background Description
Our project is going to be a web app that allows special needs people, who don't have the ability to type on their own, to search for web content based on pictures.

## Project Problem Statement
There are currently special needs people who enjoy viewing web content (such as youtube videos) but have no way to access them on their own without the help of a caregiver.

## Lack of Current Solutions
Currently there are things like Google's reverse image search that allows users to upload an image to search google with however this would not be user friendly for the user base we are trying to help. We want to create a platform that has list of images that our user can click without the help of a caregiver. Thus giving independence to our user as well is streamline on of the many responsibilites for their care.

## Background Skills/Interests
This project is near and dear for one of our members, Kyle Romard. This is something he thought up in order to better help his sister, who has special needs, and his family.

In terms of technical skills, we all have a background in web development from our co-op rotations. 

## Aproach
Our approach to this problem will be to create either a web app or a hybrid mobile app that allows a caregiver to login, search for topics to add to a dashboard, display those items as pictures on the dashboard for our end user to scroll through and pick from. Once the item is picked use associated metadata to use third party search APIs to serve content to our users.

## Preliminary Goals
* Build an image search index with associated metadata
* Create a dashboard displaying a user's pinned images
* Create a simple and easy auth flow
* Create a user experience designed to resonate with our target audience
* Give independence to our users

## Stretch Goals
* Use ML to recommend topics to our users so that caregivers don't have to manually add everything themselves
* Have both a web app and a hybrid mobile app

## Expectations for Final Demo
For our final demo we plan to have everything listed in our preliminary goals section ready to demo and have a working app deployed and ready to use.


# Member Bios

# <a name="dm"> Devin McPherson

### Contact Information

* **Email:** [mcpherdpt@mail.uc.edu](mailto:mcpherdp@mail.uc.edu)
* **Phone:** (513) 405-9937
* **Personal Github:** [devinmcpherson](https://www.github.com/devinmcpherson)

### Co-op Work Experience

1. **Spring 2016** - Assurex Health
    * Began with automated test development using Selenium
    * Wrote QA utilities using .NET Framework
    * Worked on an Agile team doing web development with .NET Framework
2. **Fall 2016** - Assurex Health
    * Continued web development using .NET Framework and AngularJS
3. **Summer 2017** - Great American Insurance
    * Built automated testing framework from the ground up using C# and Selenium
4. **Spring 2018** - Disney Parks and Resorts Technologies
    * Maintained and wrote new automated tests using Python and Selenium
    * Preformed bulk data migrations using SQL and Python
    * Wrote unit tests for Our front end web ui
5. **Fall 2018** - 84.51
    * Worked with a team to develop a full-stack portal into a major internal platform from the ground up
    * Learned to use a wide variety of technologies, including Spring Boot, Angular 6 and CLI, NX, Docker, and Google Cloud Platform

# <a name="gt"> Gerhard Tetzner

### Contact Information

* **Email:** [tetznegt@mail.uc.edu](mailto:tetznegt@mail.uc.edu)
* **Phone:** 513-907-1862
* **Gitlab:** [gerhard2202](https://www.gitlab.com/gerhard2202)

### Co-op Work Experience

1. **Spring 2016** - Assurex Health
    * Began with automated test development using Selenium
    * Wrote QA utilities using .NET Framework
    * Worked on an Agile team doing web development with .NET Framework
2. **Fall 2016** - Assurex Health
    * Continued web development using .NET Framework and AngularJS
3. **Summer 2017** - Fortech
    * Maintained software written in .NET Framework using C# and VB.NET
    * Created manufacturing solutions using .NET Framework and AngularJS
4. **Spring 2018** - Fortech
    * Continued maintaining software written in .NET Framework using C#, VB.NET, and AngularJS
    * Independently developed manufacturing solutions using .NET Core, .NET Framework, C#, and React
5. **Fall 2018** - 84.51
    * Worked with a team to develop a full-stack portal into a major internal platform from the ground up
    * Learned to use a wide variety of technologies, including Spring Boot, Angular 6 and CLI, NX, Docker, and Google Cloud Platform

# <a name="mf"> Matt Flowers

### Contact Information

* **Email:** [flowermm@mail.uc.edu](mailto:flowermm@mail.uc.edu)

### Co-op Work Experience

1. Software Developer - Siemens PLM Software
  * January 2018 - August 2018
  * Worked on Teamcenter frontend using AngularJS and backend using C++.
2. DeVore Technologies - Backend Developer
  * January 2015 - January 2018
  * Developed large scale business applications using .NET Framework and C#.

# <a name="kr"> Kyle Romard

### Contact Information
* **Email:** [romardkt@mail.uc.edu](mailto:romardkt@mail.edu)
* **Phone:** 513-465-5314

### Co-op Work Experience
1. Software Developer - Fox Sports 
  * 2015-Current
  * Front end and back end programmer for fox sports
  * Handled the statistical data recieved from sporting events and populated our data base and pushed it to our app and website
  * Created user friendly web pages on our employee support site to help optimize the work flow
