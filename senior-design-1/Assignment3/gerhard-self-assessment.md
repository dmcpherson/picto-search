# Introduction

Our project is designed to be an accessible video streaming platform to those who can't navigate complex websites. It is designed with
individuals that have cognitive disabilities in mind, but it may also be applicable to other situations, such as when a parent might want
to give a young child more autonomy over their media consumption. This project is relevant to me academically as it will be a good way
to apply the wide variety of things I've been taught across my studies and work experiences. It's also relevant to me on a more personal
level, as I very much enjoy front-end development and am very interested to apply and hone my design skills in a project that requires
me to really focus on the user rather than the tech.

# College Experiences

## Classes

The courses I've taken have given me a decent variety of knowledge to draw from going into this project. As our project is a full-stack
web application, it is going to require different skills to develop its database, backend, and frontend. For the database, I can draw on
the knowledge from my Data Structures and Database Design & Development courses, the latter of which was one of the best and most insightful
classes I've taken. For the backend, I'll apply a wide variety of skills picked up in class, including object-oriented programming and 
other core skills from introductory Computer Science courses and design principles from Software Engineering. For the frontend, little
of my academic experiences will apply, but this semester does feature a User Interface course that I'm confident will provide useful knowledge.
Across the board, I will also benefit from development practices taught to me during Software Engineering and this semester's
Requirements Engineering course.

## Work Experiences

Most of the knowledge I will draw on for this project will come from my co-ops, which all involved working on full-stack web applications
and have provided me with a very wide variety of skills that will be applicable. During my first two semesters at Assurex, I learned
to navigate a monolithic web application and to develop unit and integration tests. At Fortech, I thoroughly studied the React front-end
framework and picked up a lot of front-end development skills along the way, as well as maintained other web application backends and 
built an application's database from the ground up. Lastly, at 84.51, I worked with a group of co-ops and interns to build a web
application from scratch, picking up not only good development skills but also skills surrounding the development process such as
version control management, more testing knowledge, design methodologies, and how to triage issues. These skills will help me to make sure
that the project is not only functional and well-developed, but thoroughly tested and built using efficient and effective methodologies.

# Motivation & Preliminary Approach

I am motivated to participate on this project on a personal level because I very much enjoy frontend development and I am personally
very interested in applying and honing my skills for designing and implementing an effective user experience. I'm very much looking forward
to designing an interface for users that aren't often considered when developing online experiences, and I feel that making a good online
experience for these users will be a worthwhile endeavor both in terms of personal development and in terms of creating a useful
piece of software.

Our approach to designing our solution will be done by keeping two users in mind - a user responsible for management and setup of an
account, and primary users that are the actual content consumers. The design of the application will focus on ease of use and accessibility
above feature richness, as we think there are already plenty of highly feature rich platforms, but no accessible platform for users
that are not equipped to use those feature rich interfaces.

We expect to design an application that would accomplish two goals: first, we will ease the work that needs to be done by caregivers
or other individuals responsible for managing the media consumption of our primary users. Second, arguably the more important goal,
we will deliver an accessible experience to users who aren't cognitively equipped to navigate a complicated website like YouTube,
giving them a steady stream of the content they want to see and some agency over what they watch.

As for evaluating our contributions, we plan on using an agile methodology - likely Kanban - where individual developers will take
ownership of user stories and work on them to completion, giving us a clean way to keep track of who's accomplished what. This will
also allow us to track our progress towards completion, as we'll know when we're ready when all of our broader stories and epics are
completed.
