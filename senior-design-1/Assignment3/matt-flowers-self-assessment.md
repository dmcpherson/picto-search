# Matt Flowers Self-Assessment

## Introduction

The Picto-Search project is going to be a full-stack web application that will make watching and finding YouTube videos easier for people that may have trouble doing so. This user group includes users with intellectual or cognitive disabilities, or very young users. The Picto-Search works by providing a user a grid of images that have been chosen and set by the Account Administrator. These images will have search terms and keywords associated with them. When a user selects one of these images, the YouTube API will be called with those keywords and return back videos that may be of interest to the user.

## Collective College Experiences

I have taken many classes throughout my time in college that will help with the development of this project. My Database Design and Development class showed me the correct way to design schema for complex database systems. This was useful because our application will be storing data in a database with multiple relationships. Another important part of this project is creating an easy to use user interface. I have taken a user interface class that taught me common practices and ways to make a UI truly easy to use and understand.

Though my classes have been important, my co-op experience has really helped me gain knowledge that will be useful while working on this project. Each of my terms working as a co-op I have been designing websites and writing complex back-end logic for these sites. Since our project will be a full-stack web application, I will be able to apply most of the things that I have learned while working. I have designed databases and the supporting backed logic for large scale business applications. I have also created the front-end for those applications in order to easily display data to the user, as well as provide control over that data to the user.

## Motivation and Preliminary Approach

I am motivated to work on this project because it is unlike any other large project that I have worked on before. I have either worked on projects meant to benefit large companies or clients, and most of my personal projects are created to benefit other open-source programmers. With this project, my team and I will hopefully be able to make a product that will make individual's lives easier than it previously was. Our prelimiary approach to designing the solution was to think about how to make watching YouTube videos easier for the intended user-base. Through this we were able to come up with an idea for an interface that will solve that problem.

My expected results and accomplishments are to fully create the web application and release it to the public. Getting the application deployed publicly will be a large accomplishment. Also, finding a user group to begin using the software and providing feedback will be a large accomplishment. I also believe that the biggest accomplishment would be seeing reoccurring users continue to choose to use our application because it solves the original problem we created it for. We will continue to work on the application until all of the features we have promised have been implemented. If we still have time, we also have stretch goals that we would like to implement as well. We will continue to work on this until we have a production ready product that can be released to the public and used by anyone.
