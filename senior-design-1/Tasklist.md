# Task List


| Task Number | Task Statement                                           | Assigned To   | Hour Estimate |
|-------------|----------------------------------------------------------|---------------|---------------|
| 1           | Setup web server                                         | Matt          | 5             |
| 2           | Setup front end                                          | Matt          | 5             |
| 3           | Setup CI/CD pipeline                                     | Devin         | 8             |
| 4           | Set up database                                          | Kyle          | 4             |
| 5           | Create/maintain project backlog                          | Devin         | 12            |
| 6           | Research different architecture paradigms                | All           | 4             |
| 7           | Decide on our architecture                               | All           | 1             |
| 8           | Create database schema                                   | All           | 6             |
| 9           | Create auth service                                      | Gerhard       | 12            |
| 10          | Build auth view                                          | Gerhard       | 8             |
| 11          | Create user roles                                        | Gerhard       | 4             |
| 12          | Build user home screen (include our separate components) | Devin         | 16            |
| 13          | Build profile selection view                             | Gerhard       | 8             |
| 14          | Build profile management view                            | Gerhard       | 12            |
| 15          | Build search service                                     | Matt and Kyle | 16            |
| 16          | Build Youtube api gateway                                | Matt and Kyle | 12            |
| 17          | Build Pexels api gateway                                 | Matt and Kyle | 8             |
| 18          | Build search ui component                                | Devin         | 6             |
| 19          | Build playlist service                                   | Devin         | 4             |
| 20          | Build player ui                                          | Kyle, Matt    | 6             |
