# Milestones

## 1. Setting Up Project Skeleton

There will be a milestone when the entire skeleton of our project is set up. Once it is set up we will be able to begin working on specific features of the application.

#### Deliverables for this Milestone

* Setting up web server
  * This is a deliverable since it will provide us with a web server to begin adding features to.
* Setting up front-end client
  * This is a deliverable since it will provide us with a front-end client to be adding features to.
* Setting up database
  * This is a deliverable since it will provide us with a database to being using while developing the rest of the application.
* Setting up CI/CD
  * This is a deliverable because it will provide us with continuous integration testing and continuous delivery to make deploying changes easier throughout our time developing the application.
  
## 2. Complete Profile Management/Selection

This is a milestone because the profile management and profile selection features are a key part of our application. When those are completed, account managers will be able to create profiles and configure them.

#### Deliverables for this Milestone

* Creating database schema
    * This is a deliverable because the account manager and profile schema will be important for retrieving this information from the database.
* Creating user roles
    * This is a deliverable because the roles associated with account manager will allow them to create new profiles and update the configuration for those profiles.
* Creating auth service
    * This is a deliverable because the auth service will be what actually allows the user to log in to the application.
* Building profile selection view
    * This is a deliverable because the selection view will be the main interface the user sees when they open the application.
* Building profile management view
    * This is a deliverable because the management view will be how the account manager updates settings for the multiple profiles associated with their account.
    
## 3. Releasing the Application

This is a milestone because it will be actually releasing our finished product. All of our tasks and previous milestones lead up to this one.

#### Deliverables for this Milestone

* Complete all server related tasks
    * This is a deliverable because when it is finished we will have all server logic complete that can be tied to the front-end.
* Complete all database related tasks
    * This is a deliverable because when it is completed we will have the database working properly for when our server runs operations on the data in it.
* Complete all UI related tasks
    * This is a deliverable because when it is completed there will be an interface for each sections of the application that will make it so the user is able to use the product.
* Complete all testing
    * This is a deliverable because it will ensure that our product is working and in a proper state to be released.
