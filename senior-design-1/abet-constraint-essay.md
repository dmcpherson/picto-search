# ABET Constraint Essay

## Economic

Our project is definitely going to be financially constrained - without any significant budget, we'll be dependent on free and/or open
source solutions to our problems. Thankfully, there is a huge selection of these to pick from - for our code repository, we naturally have
the university GitHub. For continuous integration and testing, we can take advantage of our student licenses with JetBrains to leverage
TeamCity or we can set up an additional Git remote with GitLab and do our continuous integration through them. Our largest hurdle to overcome
will be a hosting solution, with options such as Heroku for a totally free solution or Digital Ocean for a very cheap solution that could
be handled out of pocket. While we don't fear that economic limitations will cripple our project, they have caused us to be humble with our
scope and design decisions.

## Ethical

One of our project's more obvious constraints is that we need to take extra care when designing the system for a userbase that is potentially
very sensitive to poor design decisions. Without taking extra care to ensure that this delivers a positive experience, it is very possible
for us to frustrate and upset our primary users as well as complicating the lives of caregivers. An interesting issue we will also have to
deal with is ethically testing our interfaces, as it would not be ethical to present a frustrating and upsetting experience to an individual
with a cognitive disability that would make it difficult for them to understand that it is a testing situation and that a poor experience
is likely or expected. We plan to work around this issue by ensuring that any changes that would affect primary users are first vetted by
a base of experienced caregivers who might know what is and isn't likely to upset the primary users they work with.

## Social

Our project is designed to enhance the quality of life of individuals who may struggle to navigate complicated interfaces like that of
YouTube as well as streamline the process of presenting content to these users for caregivers that manage the content these users consume.
As our project is designed to benefit a certain group of people, we need to ensure that we work closely with these individuals and make sure
that we are delivering something they can use and benefit from. It will be important moving forward to avoid assumptions and expectations that
one might take for granted when dealing with a more capable base of users and communicate clearly and openly to ensure that we are meeting
the requirements of our users and designing things with them in mind.

## Cultural

While our project has the potential for global use, our team of four developers are all primary English-speaking residents of the United States,
so our project will be constrained to the cultural and social expectations of users with similar cultural backgrounds. While there is research that
suggests that cognitive and developmental disabilities vary greatly between different parts of the world, it is beyond the scope of our
project and the realistic capabilities of our team to develop our application for foreign audiences. As such, we will be focusing all of
our development efforts on delivering the best experience we can to our American users. It is worth noting that, should this
application ever be commercialized or otherwise published for public use beyond our original scope, we will need to take this into consideration.
