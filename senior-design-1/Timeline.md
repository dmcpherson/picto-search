# Timeline


| Task Number | Task Statement                                           | Start Date | End Date  | Sprint  |
|-------------|----------------------------------------------------------|------------|-----------|---------|
| 1           | Setup web server                                         | 1/1/2019   | 1/14/2019 | 1       |
| 2           | Setup front end                                          | 1/1/2019   | 1/14/2019 | 1       |
| 3           | Setup CI/CD pipeline                                     | 1/1/2019   | 1/14/2019 | 1       |
| 4           | Set up database                                          | 1/1/2019   | 1/14/2019 | 1       |
|             | Milestone: Setting Up Project Skeleton                   |            | 1/14/2019 |         |
| 5           | Create/maintain project backlog                          | 1/1/2019   | 1/14/2019 | Ongoing |
| 6           | Research different architecture paradigms                | 1/15/2019  | 1/28/2019 | 2       |
| 7           | Decide on our architecture                               | 1/15/2019  | 1/28/2019 | 2       |
| 8           | Create database schema                                   | 1/15/2019  | 1/28/2019 | 2       |
| 9           | Create auth service                                      | 1/15/2019  | 1/28/2019 | 2       |
| 10          | Build auth view                                          | 2/12/2019  | 2/25/2019 | 4       |
| 11          | Create user roles                                        | 1/29/2019  | 2/11/2019 | 3       |
| 12          | Build user home screen (include our separate components) | 2/26/2019  | 3/9/2019  | 5       |
| 13          | Build profile selection view                             | 1/29/2019  | 2/11/2019 | 3       |
| 14          | Build profile management view                            | 2/26/2019  | 3/9/2019  | 5       |
|             | Milestone: Complete Profile Management/Selection         |            | 3/9/2019  |         |
| 15          | Build search service                                     | 1/15/2019  | 1/28/2019 | 2       |
| 16          | Build Youtube api gateway                                | 2/12/2019  | 2/25/2019 | 4       |
| 17          | Build Pexels api gateway                                 | 2/12/2019  | 2/25/2019 | 4       |
| 18          | Build search ui component                                | 2/12/2019  | 2/25/2019 | 4       |
| 19          | Build playlist service                                   | 1/15/2019  | 1/28/2019 | 2       |
| 20          | Build player ui                                          | 2/26/2019  | 3/9/2019  | 5       |
|             | Milestone: Release Application                           |            | 4/1/2019  |         |
