# User Stories

## US 1:
*As a*: Caregiver  
*I want*: to pick a list of video categories  
*So that*: when my user is on the homepage they have pictures to search with  

## US 2:
*As a*: User  
*I want*: have a list of pictures  
*So that*: I don't have to type  

## US 3:
*As a*: Caregiver  
*I want*: set up different profiles  
*So that*: I can differ between what my users can see  
