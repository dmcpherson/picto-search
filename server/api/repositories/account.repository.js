const {
  Account,
} = require('../models');

module.exports = {
  getAll: cb => {
    Account.find(cb);
  },
  create: (params, cb) => {
    const account = new Account(params);
    account.save(cb);
  },
  getById: (id, cb) => {
    Account.findById(id, cb);
  },
  update: (id, params, cb) => {
    params._id = id;
    Account.replaceOne({ _id: id }, params, cb);
  }
};