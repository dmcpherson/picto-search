const express = require('express');
const {
  AccountController,
  ImageController,
  YoutubeController
} = require('./controllers');
const {
  auth,
} = require('../middleware');

const api = express.Router();

api.endpoint = '/api';

api.get('/', (req, res) => {
  res.json({
    timestamp: Date.now(),
  });
});

api.use(AccountController.endpoint, AccountController);
api.use(ImageController.endpoint, ImageController);
api.use(YoutubeController.endpoint, YoutubeController)

api.get('/authorize', auth.jwtCheck, (req, res) => {
  res.json({
    message: 'Authorized',
  });
});

module.exports = api;