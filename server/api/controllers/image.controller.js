const express = require('express');
const axios = require('axios');
const {
  pexelsApiKey,
} = require('../../config');

axios.defaults.headers.common['Authorization'] = pexelsApiKey;

const pexelsUrl = 'https://api.pexels.com/v1';
const ImageController = express.Router();

ImageController.endpoint = '/images';

// query
ImageController.route('/')
  .get((req, res) => {
    const query = req.query.query;
    const perPage = req.query.perPage || 15;
    const page = req.query.page || 1;
    if (query !== undefined) {
      axios(`${pexelsUrl}/search?query=${query}&per_page=${perPage}&page=${page}`)
        .then(response => {
          res.status(200).json(response.data);
        })
        .catch(err => {
          console.error(err);
          res.status(418).json({
            message: 'Something broke',
          });
        });
    } else {
      axios(`${pexelsUrl}/curated?per_page=${perPage}&page=${page}`)
        .then(response => {
          res.status(200).json(response.data);
        })
        .catch(err => {
          console.error(err);
          res.status(418).json({
            message: 'Something broke',
          });
        });
    }
  });

ImageController.route('/:id')
  .get((req, res) => {
    const id = req.params.id;
    axios(`${pexelsUrl}/photos/${id}`)
      .then(response => {
        res.status(200).json(response.data);
      })
      .catch(err => {
        console.error(err);
        res.status(418).json({
          message: 'Something broke',
        });
      });
  });

module.exports = ImageController;