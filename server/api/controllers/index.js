module.exports = {
  AccountController: require('./account.controller'),
  ImageController: require('./image.controller'),
  YoutubeController: require('./youtube.controller')
};