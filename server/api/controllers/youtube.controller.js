const express = require('express');
const {google} = require('googleapis');
const {
  youtubeApiKey,
} = require('../../config');

const youtube = google.youtube({
  version: 'v3',
  auth: youtubeApiKey
})

const youtubeUrl = 'https://www.googleapis.com/youtube/v3';
const YoutubeController = express.Router();

YoutubeController.endpoint = '/yt';

YoutubeController.route('/')
  .get((req, res) => {
    query = req.query.query;
    const params = {
      part: 'snippet',
      q: query,
      maxResults: 25,
      safeSearch: 'strict',
      type: 'video'
    }
    youtube.search.list(params)
      .then(response => {
        res.json(response.data);
      })
      .catch(err => {
        console.error(err);
        res.status(418).json({
          message: 'Something broke',
        });
      });
  });

YoutubeController.route('/:id')
  .get((req, res) => {
    const id = req.params.id;
    const params = {
      part: 'snippet,contentDetails,statistics,status',
      id: id
    }
    youtube.videos.list(params)
      .then(response => {
        res.json(response.data);
      })
      .catch(err => {
        console.error(err);
        res.status(418).json({
          message: 'Something broke',
        });
      });
  });

module.exports = YoutubeController;