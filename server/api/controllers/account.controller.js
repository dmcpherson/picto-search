const express = require('express');
const {
  AccountRepository,
} = require('../repositories');
const { jwtCheck } = require('../../middleware/auth'); 
const AccountController = express.Router();

AccountController.endpoint = '/accounts';

AccountController.route('/')
  .get((req, res) => {
    AccountRepository.getAll((err, accounts) => {
      if (err) {
        res.send(err);
      } else {
        res.json(accounts);
      }
    });
  })
  .post(jwtCheck, (req, res) => {
    req.body._id = req.user.sub;
    AccountRepository.create(req.body, (err, account) => {
      if (err) {
        res.send(err);
      } else {
        res.status(200);
        res.json(account);
      }
    });
  })
  .put(jwtCheck, (req, res) => {
    AccountRepository.update(req.user.sub, req.body, (err, accounts) => {
      res.status(200).end();
    });
  });

AccountController.route('/authorize')
  .get(jwtCheck, (req, res) => {
    AccountRepository.getById(req.user.sub, (err, account) => {
      if (err) {
        res.send(err);
      } else {
        if (account !== null) {
          res.status(200).json(account);
        } else {
          res.status(404).end();
        }
      }
    });
  });

AccountController.route('/:id')
  .get((req, res) => {
    AccountRepository.getById(req.params.id, (err, account) => {
      if (err) {
        res.send(err);
      } else {
        if (account !== null) {
          res.status(200).json(account);
        } else {
          res.status(404).end();
        }
      }
    });
  })
  .put(jwtCheck, (req, res) => {
    AccountRepository.update(req.params.id, req.body, (err, account) => {
      // Only send a 200 status because err and account
      // are always null for some reason.
      res.status(200).end();
    });
  });

module.exports = AccountController;