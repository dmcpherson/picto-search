const mongoose = require('mongoose');
const Video = require('./video.model');

const playlistSchema = new mongoose.Schema({
  name: String,
  image: String,
  videos: [Video.schema],
}, { _id: false });

module.exports = mongoose.model('Playlist', playlistSchema);