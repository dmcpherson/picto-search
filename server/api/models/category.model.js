const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
  name: String,
  image: String,
  safeSearch: Boolean,
  keywords: [String],
}, { _id: false });

module.exports = mongoose.model('Category', categorySchema);