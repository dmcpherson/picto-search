const mongoose = require('mongoose');
const Profile = require('./profile.model');

const accountSchema = new mongoose.Schema({
  profiles: [Profile.schema],
  _id: { type: String, required: true },
}, { _id: false });

module.exports = mongoose.model('Account', accountSchema);