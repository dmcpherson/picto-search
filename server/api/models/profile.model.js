const mongoose = require('mongoose');
const Category = require('./category.model');
const Playlist = require('./playlist.model');

const profileSchema = new mongoose.Schema({
  name: String,
  avatar: Number,
  categories: [Category.schema],
  playlists: [Playlist.schema],
  favoritePlaylist: Playlist.schema,
}, { _id: false });

module.exports = mongoose.model('Profile', profileSchema);
