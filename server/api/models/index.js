module.exports = {
  Profile: require('./profile.model'),
  Account: require('./account.model'),
  Playlist: require('./playlist.model'),
  Category: require('./category.model'),
};