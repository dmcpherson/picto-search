const mongoose = require('mongoose');

const videoSchema = new mongoose.Schema({
  title: String,
  videoId: String,
}, { _id: false });

module.exports = mongoose.model('Video', videoSchema);