require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const api = require('./api');
const path = require('path');
const {
  port,
  mongoUri,
  publicPath,
} = require('./config');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(express.static(path.resolve(__dirname, publicPath)));

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, publicPath, 'index.html'));
});

app.use(api.endpoint, api);

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, publicPath, 'index.html'));
});

app.all('*', (req, res) => {
  res.status(404).json({
    message: 'Not Found',
  });
});

mongoose.connect(mongoUri, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => {
  app.listen(port, () => {
    console.log(`Server running on port ${port}`);
  });
});