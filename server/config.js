module.exports = {
  port: process.env.PORT || 3001,
  baseUrl: process.env.BASE_URL || 'api',
  mongoUri: process.env.MONGO_URI || 'mongodb://localhost:27017/app',
  pexelsApiKey: process.env.PEXELS_API_KEY,
  youtubeApiKey: process.env.YOUTUBE_API_KEY,
  publicPath: 'web-ui/build',
};