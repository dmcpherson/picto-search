import React from 'react';
import { Box } from 'grommet';
import Profile from './Profile';
import { UserSettings, Logout } from 'grommet-icons';
import history from '../auth/History';
import './Container.css';
import AddProfile from './AddProfile';

const Container = ({ profiles, auth }) =>
<Box className="profiles-container" fill>
  <Box className="home-menu" margin="medium" direction="row">
    <Box className="home-menu-button user-settings" onClick={() =>  history.push("/settings") }>
      <UserSettings color={"#55a0cc"} size="large"/>
    </Box>
    <Box className="home-menu-button logout" onClick={() => auth.logout() }>
      <Logout color={"#ce3737"} size="large"/>
    </Box>
  </Box>
  <Box margin="auto auto 0 auto" className="profile-header" textAlign="center">
    <h1>Welcome!</h1>
    <h2>{ profiles.length ? "Select a profile" : "Let's get started" }</h2>
  </Box>
  <Box margin="0 auto auto auto" className="profiles" direction="row">
    { profiles.length ?
      profiles.map((profile, index) => {
        return <Profile key={index} profile={profile}/>
      }) :
      <AddProfile onClick={()=> history.push("/settings")}/>
    }
  </Box>
</Box>

export default Container;