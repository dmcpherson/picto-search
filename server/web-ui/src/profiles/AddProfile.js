import React from 'react';
import { Box } from 'grommet';
import { Add } from 'grommet-icons';
import './AddProfile.css';

const AddProfile = ({ onClick }) =>
<Box className="add-profile" margin="medium" width="200px" height="200px" onClick={onClick}>
  <Add size="xlarge" color="#aaa"/>
</Box>

export default AddProfile;