import React from 'react';
import { Box, Image } from 'grommet';
import { Link } from 'react-router-dom';
import profilePictures from '../images/profilePictures';
import './Profile.css';
import history from '../auth/History.js';

const Profile = ({ profile }) =>
  <Box className="profile-container" direction="column" align="center" textAlign="center" onClick={() => history.push(`/profile/${profile.name}`)}>
    <Box className="profile" margin="medium" width="200px" height="200px">
      <Image fit="contain" src={profilePictures[profile.avatar]}/>
    </Box>
    <h2>{profile.name}</h2>
  </Box>

export default Profile;