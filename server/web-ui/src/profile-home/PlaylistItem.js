import React from 'react';
import { Box, Heading, Image } from 'grommet';
import { Link } from 'react-router-dom';

 const getYoutubeThumbnail = (videoId) => {
    return `https://img.youtube.com/vi/${videoId}/mqdefault.jpg`;
 }

const PlaylistItem = ({playlist, profile}) => 
<Box flex={false} basis="20%" margin="medium" border={{ color: "#444", style: "solid", size:"4px", side: "all" }} round="4px">
    <Link to={`${profile.name}/playlist/${playlist.name}`}> 
        <Box style={{ position: "relative" }}>
            <div style={{ 
                width: "100%", 
                height: "0", 
                padding: "56.25% 0 0 0", 
                backgroundImage: `url(${getYoutubeThumbnail(playlist.videos[0].videoId)})`,
                backgroundPosition: "center",
                backgroundSize: "cover"
            }}></div>
            {/* {<Image
                fit="cover"
                src={category.image} 
            />} */}
        </Box>
    </Link>
</ Box>

export default PlaylistItem;