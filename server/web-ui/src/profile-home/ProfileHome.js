import React from 'react';
import { Box, Heading } from 'grommet';
import { Logout } from 'grommet-icons';
import CategoryItem from './CategoryItem';
import PlaylistItem from './PlaylistItem';
import history from '../auth/History';
import './ProfileHome.css';

const ProfileHome = ({ profile }) =>
<Box fill>
    <Box margin="medium" flex={false} direction="row" justify="between" align="center">
        <Heading margin="none" level="2">Hi, {profile.name}!</Heading>
        <Box direction="row">
            <Box className="profile-menu-button logout" onClick={ () => history.push("/home") }>
                <Logout color={"#ce3737"} size="large"/>
            </Box>
        </Box>
    </Box>
    <Box flex="grow" justify="around">
        { profile.categories.length ? <Box flex={false} align="center">
            <Heading level="2">Categories</Heading>  
            <Box direction="row" flex={true} justify="around" width="100%" wrap={true}>
                { profile.categories.map((category, index) => <CategoryItem key={index} category={category} profile={profile} />) }
            </Box>
        </Box> : null }
        { profile.playlists.length ? <Box flex={false} align="center">
            <Heading level="2">Playlists</Heading>
            <Box direction="row" flex={true} justify="around" width="100%" wrap={true}>
                { profile.playlists.map((playlist, index) => <PlaylistItem key={index} playlist={playlist} profile={profile} />) }
            </Box>
        </Box> : null }
    </Box>
</Box>


export default ProfileHome;

    // <Grid
    //     rows={['xxsmall', 'xsmall', 'medium', 'xxsmall', 'xsmall', 'medium', 'xxsmall']}
    //     columns={['flex']}
    //     gap="large"
    //     areas={[
    //         { name: 'header', start: [0, 0], end: [1, 0] },
    //         { name: 'section-title-1', start: [0, 1], end: [1, 1] },
    //         { name: 'section-content-1', start: [0, 2], end: [1, 2] },
    //         { name: 'expand-btn-1', start: [0, 3], end: [1, 3] },
    //         { name: 'section-title-2', start: [0, 4], end: [1, 4] },
    //         { name: 'section-content-2', start: [0, 5], end: [1, 5] },
    //         { name: 'expand-btn-2', start: [0, 6], end: [1, 6] },
    //     ]}
    // >
    //     <Box gridArea="header" background="dark-2" justify="between" direction="row" align="center">
    //     </Box>
    //     <Heading gridArea="section-title-1" color="red" level="1" responsive="true">
    //         Categories
    //     </Heading>
    //     <Box gridArea="section-content-1" background="light-1" direction="row" justify="between">
    //         { profile.categories.map((category, index) => {
    //             return <CategoryItem key={index} category={category} profile={profile} />
    //         })}
    //     </Box>
    //     <Box gridArea="expand-btn-1">
    //         <Button
    //             color="red"
    //             fill={false}
    //             label
    //             alignSelf="center"
    //             icon={<LinkDown />}
    //             onClick={() => { }}
    //         />
    //     </Box>
    //     <Heading gridArea="section-title-2" color="red" level="1" responsive="true">
    //         Playlists
    //     </Heading>
    //     <Box gridArea="section-content-2" background="light-2" direction="row" justify="between">
    //         { profile.playlists.map((playlist, index) => {
    //             return <PlaylistItem key={index} playlist={playlist} profile={profile} />
    //         })}
    //     </Box>
    //     <Box gridArea="expand-btn-2">
    //         <Button
    //             color="red"
    //             fill={false}
    //             label
    //             alignSelf="center"
    //             icon={<LinkDown />}
    //             onClick={() => { }}
    //         />
    //     </Box>
    // </Grid> 