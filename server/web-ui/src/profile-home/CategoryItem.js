import React from 'react';
import { Box, Image } from 'grommet';
import { Link } from 'react-router-dom';

const CategoryItem = ({category, profile}) => 
<Box flex={false} basis="20%" margin="medium" border={{ color: "#444", style: "solid", size:"4px", side: "all" }} round="4px">
    <Link to={`${profile.name}/category/${category.name}`}> 
        <Box style={{ position: "relative" }}>
            <div style={{ 
                width: "100%", 
                height: "0", 
                padding: "56.25% 0 0 0", 
                backgroundImage: `url(${category.image})`,
                backgroundPosition: "center",
                backgroundSize: "cover"
            }}></div>
            {/* {<Image
                fit="cover"
                src={category.image} 
            />} */}
        </Box>
    </Link>
</ Box>
    
export default CategoryItem;

/* <Link to={`${profile.name}/category/${category.title}`}> 
    <Box 
        elevation="medium" 
        margin="large" 
        pad="medium" 
        flex="grow" 
        background="dark"
    >
        <Image fit="cover" src={category.image} />
    </Box>
</Link> */