import React from 'react';
import { compose, withHandlers, withProps } from 'recompose';
import { Box, Form, FormField, TextInput, Image, Heading, Tabs, Tab, Button } from 'grommet';
import profilePictures from '../images/profilePictures';
import CategorySettings from './category-settings/CategorySettings';
import PlaylistSettings from './playlist-settings/PlaylistSettings';

const NameInput = ({ name, setName, saveChanges }) => {
  return <Box width="25%">
    <TextInput
      placeholder="Name"
      size="xlarge"
      value={name}
      onChange={(event) => setName(event.target.value)}
      onBlur={saveChanges}
    />
  </Box>
}

const AvatarSelect = ({ avatar, setAvatar }) => {
  const borderStyles = {
    color: "white",
    size: "medium",
    style: "solid",
    side: "all"
  }
  
  const selectedBorderStyles = {
    ...borderStyles,
    color: "#9edbff"
  }

  return <Box width="100%" align="center" pad="medium">
    <Heading margin="xsmall" level="3">Select a profile picture</Heading>
    <Box align="center" direction="row" justify="between">
      { profilePictures.map((picture, index) => 
        <Box 
          border={ avatar === index ? selectedBorderStyles : borderStyles} 
          round="small" 
          overflow="hidden"
          key={index}
          onClick={() => setAvatar(index)}
        >
          <Box width="75px" height="75px">
            <Image
              fit="contain"
              src={picture}
            />
          </Box>
        </Box>
      )}
    </Box>
  </Box>
}

const TabSelect = ({profile, updateProfile}) => {
  return <Tabs width="100%" flex="grow" pad="medium">
    <Tab title="Categories">
      <CategorySettings profile={profile} updateProfile={updateProfile} />
    </Tab>
    <Tab title="Playlists">
      <PlaylistSettings profile={profile} updateProfile={updateProfile} />
    </Tab>
  </Tabs>
}

const baseProfileDetails = ({ profile, updateProfile, setName, setAvatar, saveChanges }) => {
  return <Box width="xlarge">
    {
      profile ? 
      <Box flex="grow" align="center">
        <NameInput name={profile.name} setName={setName} saveChanges={saveChanges}/>
        <AvatarSelect avatar={profile.avatar} setAvatar={setAvatar}/>
        <TabSelect profile={profile} updateProfile={updateProfile}/>
        <Button label="Save" onClick={saveChanges}/>
      </Box> :
      <Box align="center">
        <Heading level="2">Add or select a profile</Heading>
      </Box>
  }
  </Box>
}

const ProfileDetails = compose(
  withHandlers({
    setName: ({ profile, updateProfile }) => (name) => updateProfile({ ...profile, name }),
    setAvatar: ({ profile, updateProfile }) => (avatar) => updateProfile({ ...profile, avatar }),
    saveChanges: ({ profile, profiles, selectedProfile, updateAccount }) => () => {
      const newProfiles = profiles.slice();
      newProfiles[selectedProfile] = profile;
      updateAccount({ profiles: newProfiles });
    }
  })
)(baseProfileDetails);

export default ProfileDetails;