import React from 'react';
import { Box, TextInput, Button, Text } from 'grommet';
import { Search, Close, Add } from 'grommet-icons';
import { compose, withState, withHandlers } from 'recompose';
import axios from 'axios';
import PlaylistImage from './PlaylistImage';

const ImageSearchInput = ({searchText, setSearchText}) => {
    return <TextInput
        placeholder="Search Videos..."
        size="large"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
    />
}

const baseImagePicker = ({setShow, handleSelectedAdd, category, updateCategory, videos, searchText, setSearchText, onSearch, onChosenToggle}) => {
    return (
        <Box flex={true}>
            <Box direction="row">
                <ImageSearchInput searchText={searchText} setSearchText={setSearchText} />
                <Button 
                    color="red"
                    margin="10px"
                    label="Search"
                    icon={<Search color="red"/>}
                    onClick={() => onSearch()}
                />
            </Box>
            <Box direction="row" margin="small" flex={true} justify="around" width="100%" wrap={true} overflow={{vertical: "scroll", horizontal:"auto"}}>
                { 
                    videos.map((video, index) => 
                    <PlaylistImage key={index} index={index}
                        onClick={() => onChosenToggle(!video.selected, index)}
                        selected={video.selected}
                        backgroundUrl={video.snippet.thumbnails.default.url}
                        boxProps={{width:"30%", margin:"5px", style:{ position: "relative" }}}
                    >
                        <Text align="center">{video.snippet.title}</Text>
                    </PlaylistImage>) 
                }
            </Box>
            <Box direction="row" justify="around">
                <Button color="red" label="Add These Videos" icon={<Add size="medium" color="red" />} onClick={handleSelectedAdd} />
                <Button color="red" label="Close" icon={<Close size="medium" color="red" />} onClick={() => setShow(false)} />
            </Box>
        </Box>
    )
}

const ImagePicker = compose(
    withState("searchText", "setSearchText", ""),
    withState("videos", "setVideos", []),
    withHandlers({
        onSearch: ({searchText, videos, setVideos}) => () => {
            axios.get(`api/yt?query=${encodeURIComponent(searchText)}`)
            .then((response) => {
                setVideos(response.data.items);
            })
            .catch((error) => {
              // handle error
              console.log(error);
            });
        },
        onChosenToggle: ({videos, setVideos}) => (selected, index) => {
            const newVideos = videos.slice();
            newVideos[index].selected = selected;
            setVideos(newVideos);
        },
        handleSelectedAdd: ({updatePlaylist, playlist, videos, setSearchText, setVideos, setShow}) => () => {
            const newVideos = playlist.videos.slice();
            videos.forEach(video => {
                if (video.selected) {
                    newVideos.push({ videoId: video.id.videoId, title: video.snippet.title });
                }
            })
            updatePlaylist({ ...playlist, videos: newVideos});
            setSearchText('');
            setVideos([]);
            setShow(false);
        }
    })
)(baseImagePicker);

export default ImagePicker;