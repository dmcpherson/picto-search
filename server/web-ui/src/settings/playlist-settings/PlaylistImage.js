import React from 'react';
import {Box} from 'grommet';

const PlaylistImage = ({backgroundUrl, selected, onClick, children, boxProps}) => {
  return <Box onClick={onClick} { ...boxProps }>
      <div style={{ 
          width: "100%", 
          height: "0", 
          padding: "56.25% 0 0 0", 
          backgroundImage: `url(${backgroundUrl})`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          borderRadius: 8,
          border: selected ? '4px solid #f00' : '4px solid white'
      }}></div>
      {children}
  </Box>
}

export default PlaylistImage;