import React from 'react';
import { Box, TextInput, Heading, Button, Text } from 'grommet';
import { compose, withHandlers, withState, lifecycle } from 'recompose';
import PlaylistSearch from './PlaylistSearch';
import PlaylistImage from './PlaylistImage';
import { Trash } from 'grommet-icons';
import axios from 'axios';
import _ from 'lodash';

const PlaylistNameInput = ({ name, setName, saveChanges }) => {
  return <Box width="60%">
    <TextInput
      placeholder="Playlist"
      size="large"
      value={name}
      onChange={(event) => setName(event.target.value)}
      onBlur={saveChanges}
    />
  </Box>
}

const basePlaylistDetails = ({ playlist, setName, saveChanges, removeVideo, selectedPlaylist, setKeywords, updatePlaylist, searchText, setSearchText, onSearch }) => {
  return playlist ? 
  <Box margin="medium" flex={true}>
    <Box flex="grow" align="center">
      <Box width="60%" direction="row" align="center" justify="between">
        <PlaylistSearch playlist={playlist} updatePlaylist={updatePlaylist} />
        <PlaylistNameInput name={playlist.name} setName={setName} saveChanges={saveChanges} />
      </Box>
      <Box margin="medium" alignSelf="stretch" direction="row" wrap={true} justify="around">
        {playlist.videos.map((video, index) => (
          <Box margin="xsmall" style={{ position: "relative" }} maxWidth="30%" width="30%" key={index}>
            <PlaylistImage selected={false} backgroundUrl={`https://img.youtube.com/vi/${video.videoId}/mqdefault.jpg`}>
              <Text align="center">{video.title}</Text>
            </PlaylistImage>
            <Box pad="4px" style={{ position: "absolute", top: "10px", right: "4px", backgroundColor: "white", borderRadius: "4px", overflow: "hidden" }} onClick={(id) => removeVideo(id)}>
              <Trash color="#e84040"/>
            </Box>
          </Box>
        ))}
      </Box>
    </Box>
  </Box> :
  <Box width="100%" margin="small">
    <Heading level="3">Select or add a video</Heading>
  </Box>
}

const PlaylistDetails = compose(
    withState("searchText", "setSearchText", ""),
    withHandlers({
      setName: ({ playlist, updatePlaylist }) => (name) => updatePlaylist({ ...playlist, name}),
      removeVideo: ({ playlist, updatePlaylist }) => (index) => {
        const videos = playlist.videos.slice();
        videos.splice(index, 1);
        updatePlaylist({ ...playlist, videos });
      },
      saveChanges: ({ playlist, playlists, selectedPlaylist, updateProfile }) => () => {
        const newPlaylists = playlists.slice();
        newPlaylists[selectedPlaylist] = playlist;
        updateProfile({ playlists: newPlaylists });
      }
    }),
    lifecycle({
      componentDidUpdate(prevProps) {
        if (!prevProps.playlist || !this.props.playlist) return;
        if (!_.isEqual(prevProps.playlist.videos, this.props.playlist.videos)) this.props.saveChanges();
      }
    })
)(basePlaylistDetails);

export default PlaylistDetails;