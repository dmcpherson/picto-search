import React from 'react';
import { Box, Heading } from 'grommet';
import { AddCircle } from 'grommet-icons';

const getBlankPlaylist = () => ({
    name: "New Playlist",
    videos: [],
});

const SelectPlaylist = ({ playlists, selectPlaylist, addPlaylist }) => 
<Box flex={false} width="small" border={{ color: "brand", side: "right", size: "small"}}>
    { playlists.map((playlist, index) =>
        <Box 
            className="profile-label" 
            margin="small" 
            pad="small" 
            direction="row" 
            align="center" 
            key={index} 
            onClick={() => selectPlaylist(index)}
        >
            <Heading margin="0 0 0 8px" level="4">{playlist.name ? playlist.name : "New Playlist"}</Heading>
        </Box>
    )}
    <Box 
        className="profile-label" 
        margin="small" 
        pad="small" 
        direction="row" 
        align="center" 
        onClick={() => addPlaylist(getBlankPlaylist())}
    >
        <AddCircle color="black"/>
        <Heading margin="0 0 0 8px" level="4">Add Playlist</Heading>
    </Box>
</Box>

export default SelectPlaylist;