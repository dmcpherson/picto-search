import React from 'react';
import { Layer, Box, Button, TextInput } from 'grommet';
import { compose, withState, withHandlers } from 'recompose';
import axios from 'axios';
import ImagePicker from './ImagePicker';
import { Add } from 'grommet-icons';

const SearchInput = ({ searchText, setSearchText }) => {
  return <TextInput
    placeholder="Video Name"
    size="large"
    value={searchText}
    onChange={(event) => setSearchText(event.target.value)}
  />
}

const defaultPlaylistSearch = ({playlist, updatePlaylist, searchText, setSearchText, onSearch, show, setShow, videos }) => (
  <Box width="30%">
      <Button icon={<Add size="large" color="red" />} label="Add Videos" onClick={() => setShow(true)} />
      {show && (
        <Layer
          style={{
            height: '75%',
            width: '50%',
          }}
          onEsc={() => setShow(false)}
          onClickOutside={() => setShow(false)}
        >
          <Box flex={true} margin="medium">
            <ImagePicker playlist={playlist} updatePlaylist={updatePlaylist} setShow={setShow}/>
          </Box>
        </Layer>
      )}
  </Box>
);

const PlaylistSearch = compose(
  withState("show", "setShow", false),
  withState("videos", "setVideos", []),
  withState("searchText", "setSearchText", ""),
  withHandlers({
      onSearch: ({ searchText, setVideos }) => () => {
        axios.get(`/api/yt?query=${encodeURIComponent(searchText)}`).then(response => {
          setVideos(response.data.items);
        });
      }
  }),
)(defaultPlaylistSearch);

export default PlaylistSearch;