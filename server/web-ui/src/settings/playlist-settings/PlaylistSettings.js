import React from 'react';
import { withState, withHandlers, compose, lifecycle } from 'recompose';
import { Box, Layer, Heading, Button } from 'grommet';
import { Add } from 'grommet-icons';
import './PlaylistSettings.css'
import SelectPlaylist from './SelectPlaylist';
import PlaylistDetails from './PlaylistDetails';

const basePlaylistSettings = ({profile, updateProfile, addPlaylist, selectedPlaylist, selectPlaylist, playlist, updatePlaylist}) => {
    return <Box fill>
        <Box justify="start" flex="grow" direction="row">
            <SelectPlaylist
                playlists={profile.playlists}
                selectedPlaylist={selectedPlaylist}
                selectPlaylist={selectPlaylist}
                addPlaylist={addPlaylist}
            /> 
            <PlaylistDetails
                playlists={profile.playlists}
                profile={profile}
                playlist={playlist}
                updatePlaylist={updatePlaylist}
                selectedPlaylist={selectedPlaylist}
                updatePlaylist={updatePlaylist}
                updateProfile={updateProfile}
            />
        </Box>
    </Box>
}

const PlaylistSettings = compose(
    withState( "selectedPlaylist", "setSelectedPlaylist", 0),
    withState( "playlist", "setPlaylist", ({ profile, selectedPlaylist }) => profile.playlists[selectedPlaylist]),
    withHandlers({
        selectPlaylist: ({profile, selectedPlaylist, setSelectedPlaylist, setPlaylist}) => (index) => {
            if (index !== selectedPlaylist) {
                setSelectedPlaylist(index);
                setPlaylist(profile.playlists[index]);
            }
        },
        addPlaylist: ({ profile, updateProfile, setPlaylist, setSelectedPlaylist }) => (newPlaylist) => {
            const newPlaylists = [ ...profile.playlists, newPlaylist];
            updateProfile({playlists: newPlaylists});
            setPlaylist(newPlaylist);
            setSelectedPlaylist(newPlaylists.length - 1);
        },
        updatePlaylist: ({ playlist, setPlaylist }) => ( newData ) => setPlaylist({ ...playlist, ...newData })
    }),
    lifecycle({
        componentDidUpdate(prevProps) {
            if (prevProps.profile.name !== this.props.profile.name) {
                this.props.setPlaylist( this.props.profile.playlists.length ? this.props.profile.playlists[0] : null );
            }
        }
    })
)(basePlaylistSettings);

export default PlaylistSettings;


    // return <Box align="center">
    //     <Box direction="column" flex={true} justify="around" height="100%" wrap={true}>
    //         { categories.map((category, index) => <CategorySettingsItem key={index} category={category} />) }
    //     </Box>
    //     <Button 
    //         icon={<Add />}
    //         label="Add"
    //         >
    //     </Button>
    // </Box>

    // const CategorySettingsItem = ({ category }) => {
//     return <Box flex={false} basis="20%" margin="medium" border={{ color: "#444", style: "solid", size: "4px", side: "all" }} round="4px">
//         <Box style={{ position: "relative" }}>
//             <div style={{
//                 width: "100%", 
//                 height: "0", 
//                 padding: "56.25% 0 0 0", 
//                 backgroundImage: `url(${category.image})`,
//                 backgroundPosition: "center",
//                 backgroundSize: "cover"
//             }}></div>
//         </Box>
//         <Heading level="4">{category.title}</Heading>
//     </ Box>
// }