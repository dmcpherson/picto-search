import React from 'react';
import { Box, Heading } from 'grommet';
import { User, AddCircle } from 'grommet-icons';

const getBlankProfile = () => ({
  name: "",
  avatar: 0,
  categories: [],
  playlists: []
});

const SelectProfile = ({ profiles, addProfile, selectProfile }) => 
<Box width="small">
  <Heading margin="small" textAlign="center" level="2">Profiles</Heading>
  { profiles.map((profile, index) =>
    <Box 
      className="profile-label" 
      margin="4px" 
      pad="4px" 
      direction="row" 
      align="center" 
      key={index} 
      onClick={() => selectProfile(index)}
    >
      <User color="black"/>
      <Heading margin="0 0 0 8px" level="4">{profile.name ? profile.name : "New Profile"}</Heading>
    </Box>
  )}
  <Box 
    className="profile-label" 
    margin="4px" 
    pad="4px" 
    direction="row" 
    align="center" 
    onClick={() => addProfile(getBlankProfile())}
  >
    <AddCircle color="black"/>
    <Heading margin="0 0 0 8px" level="4">Add Profile</Heading>
  </Box>
</Box>

export default SelectProfile;