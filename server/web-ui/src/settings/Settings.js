import React from 'react';
import { withState, withHandlers, compose } from 'recompose';
import { Box } from 'grommet';
import { Close } from 'grommet-icons';
import ProfileDetails from './ProfileDetails';
import SelectProfile from './SelectProfile';
import './Settings.css';
import history from '../auth/History';

const baseSettings = ({account, updateAccount, addProfile, selectedProfile, selectProfile, profile, updateProfile }) => {
 return <Box style={{ position: "relative" }} fill>
  <Box margin="28px" className="settings-menu-button logout" onClick={() => history.push("/home") }>
    <Close color={"#ce3737"} size="large"/>
  </Box>
  <Box margin="large" justify="center" flex="grow" direction="row">
    <SelectProfile profiles={account.profiles} addProfile={addProfile} selectedProfile={selectedProfile} selectProfile={selectProfile}/>
    <ProfileDetails profiles={account.profiles} profile={profile} selectedProfile={selectedProfile} updateProfile={updateProfile} updateAccount={updateAccount} />
  </Box>
</Box>
}

const Settings = compose(
  withState( "selectedProfile", "setSelectedProfile", 0),
  withState( "profile", "setProfile", ({ account, selectedProfile }) => account.profiles[selectedProfile]),
  withHandlers({
    selectProfile: ({ account, selectedProfile, setSelectedProfile, setProfile }) => ( index ) => {
      if ( index !== selectedProfile) {
        setSelectedProfile(index);
        setProfile(account.profiles[index]);
      }
    },
    updateProfile: ({ profile, setProfile }) => ( newData ) => setProfile({ ...profile, ...newData }),
  }),
  withHandlers({
    addProfile: ({ account, updateAccount, setSelectedProfile, setProfile }) => ( newProfile ) => { 
      const newProfiles = [ ...account.profiles, newProfile ];
      updateAccount( { profiles: newProfiles } );
      setProfile(newProfile);
      setSelectedProfile(newProfiles.length - 1);
    }
  })
)(baseSettings);

export default Settings;