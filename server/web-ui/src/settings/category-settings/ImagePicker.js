import React from 'react';
import { Box, TextInput, Button } from 'grommet';
import { Search } from 'grommet-icons';
import { compose, withState, withHandlers } from 'recompose';
import axios from 'axios';
import CategoryImage from './CategoryImage';

const ImageSearchInput = ({searchText, setSearchText}) => {
    return <TextInput
        placeholder="Search Pictures..."
        size="large"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
    />
}

const baseImagePicker = ({category, updateCategory, selectImage, photos, searchText, setSearchText, onSearch}) => {
    return (
        <Box flex={true} width="100%">
            <Box direction="row">
                <ImageSearchInput searchText={searchText} setSearchText={setSearchText} />
                <Button 
                    color="red"
                    margin="10px"
                    label="Search"
                    icon={<Search color="red"/>}
                    onClick={() => onSearch()}
                />
            </Box>
            <Box direction="row" margin="small" flex={true} justify="around" width="100%" wrap={true} overflow={{vertical: "scroll", horizontal:"auto"}}>
                { photos.map((photo, index) => 
                    <CategoryImage 
                        key={index} 
                        category={category} 
                        onClick={() => selectImage(photo.src.tiny)} 
                        backgroundUrl={photo.src.tiny}
                        boxProps={{
                            width:"30%",
                            margin:"5px",
                            style:{ position: "relative" }
                        }}
                    />) 
                }
            </Box>
        </Box>
    )
}

const ImagePicker = compose(
    withState("searchText", "setSearchText", ""),
    withState("photos", "setPhotos", []),
    withHandlers({
        onSearch: ({searchText, photos, setPhotos}) => () => {
            axios.get(`api/images?query=${encodeURIComponent(searchText)}&per_page=30&page=1`)
            .then((response) => {
                setPhotos(response.data.photos);
            })
            .catch((error) => {
              // handle error
              console.log(error);
            });
        },
        selectImage: ({ updateCategory }) => (url) => updateCategory({ image: url })
    })
)(baseImagePicker);

export default ImagePicker;
