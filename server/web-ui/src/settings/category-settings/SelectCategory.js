import React from 'react';
import { Box, Heading } from 'grommet';
import { AddCircle } from 'grommet-icons';

const getBlankCategory = () => ({
    name: "New Category",
    image: "",
    safeSearch: "",
    keywords: []
});

const SelectCategory = ({ categories, selectCategory, addCategory }) => 
<Box width="small" border={{ color: "red", side: "right", size: "small"}}>
    { categories.map((category, index) =>
        <Box 
            className="profile-label" 
            margin="small" 
            pad="small" 
            direction="row" 
            align="center" 
            key={index} 
            onClick={() => selectCategory(index)}
        >
            <Heading margin="0 0 0 8px" level="4">{category.name ? category.name : "New Category"}</Heading>
        </Box>
    )}
    <Box 
        className="profile-label" 
        margin="small" 
        pad="small" 
        direction="row" 
        align="center" 
        onClick={() => addCategory(getBlankCategory())}
    >
        <AddCircle color="black"/>
        <Heading margin="0 0 0 8px" level="4">Add Category</Heading>
    </Box>
</Box>

export default SelectCategory;