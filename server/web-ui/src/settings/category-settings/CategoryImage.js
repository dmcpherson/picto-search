import React from 'react';
import { Box } from 'grommet';

const CategoryImage = ({category, backgroundUrl, onClick, boxProps}) => {
  const borderStyles = {
      color: "white",
      size: "medium",
      style: "solid",
      side: "all"
  }

  const selectedBorderStyles = {
      ...borderStyles,
      color: "#f00"
  }

  return <Box {...boxProps} border={ category.image === backgroundUrl ? selectedBorderStyles : borderStyles } onClick={onClick} round="small" overflow="hidden">
      <div style={{ 
          width: "100%", 
          height: "0", 
          padding: "56.25% 0 0 0", 
          backgroundImage: `url(${backgroundUrl})`,
          backgroundPosition: "center",
          backgroundSize: "cover"
      }}></div>
  </Box>
}

export default CategoryImage;