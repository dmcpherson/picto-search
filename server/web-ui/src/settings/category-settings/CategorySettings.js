import React from 'react';
import { withState, withHandlers, compose, lifecycle } from 'recompose';
import { Box, Layer, Heading, Button } from 'grommet';
import { Add } from 'grommet-icons';
import './CategorySettings.css'
import SelectCategory from './SelectCategory';
import CategoryDetails from './CategoryDetails';

const baseCategorySettings = ({profile, updateProfile, addCategory, selectedCategory, selectCategory, category, updateCategory}) => {
    return <Box fill>
        <Box justify="start" flex="grow" direction="row">
            <SelectCategory categories={profile.categories} selectedCategory={selectedCategory} selectCategory={selectCategory} addCategory={addCategory}/> 
            <CategoryDetails categories={profile.categories} category={category} updateCategory={updateCategory} selectedCategory={selectedCategory} updateCategory={updateCategory} updateProfile={updateProfile}/>
        </Box>
    </Box>
}

const CategorySettings = compose(
    withState( "selectedCategory", "setSelectedCategory", 0),
    withState( "category", "setCategory", ({ profile, selectedCategory }) => profile.categories[selectedCategory]),
    withHandlers({
        selectCategory: ({profile, selectedCategory, setSelectedCategory, setCategory}) => (index) => {
            if (index !== selectedCategory) {
                setSelectedCategory(index);
                setCategory(profile.categories[index]);
            }
        },
        addCategory: ({ profile, updateProfile, setCategory, setSelectedCategory }) => (newCategory) => {
            const newCategories = [ ...profile.categories, newCategory];
            updateProfile({categories: newCategories});
            setCategory(newCategory);
            setSelectedCategory(newCategories.length - 1);
        },
        updateCategory: ({ category, setCategory }) => ( newData ) => setCategory({ ...category, ...newData })
    }),
    lifecycle({
        componentDidUpdate(prevProps) {
            if (prevProps.profile.name !== this.props.profile.name) {
                this.props.setCategory( this.props.profile.categories.length ? this.props.profile.categories[0] : null );
            }
        }
    })
)(baseCategorySettings);

export default CategorySettings;


    // return <Box align="center">
    //     <Box direction="column" flex={true} justify="around" height="100%" wrap={true}>
    //         { categories.map((category, index) => <CategorySettingsItem key={index} category={category} />) }
    //     </Box>
    //     <Button 
    //         icon={<Add />}
    //         label="Add"
    //         >
    //     </Button>
    // </Box>

    // const CategorySettingsItem = ({ category }) => {
//     return <Box flex={false} basis="20%" margin="medium" border={{ color: "#444", style: "solid", size: "4px", side: "all" }} round="4px">
//         <Box style={{ position: "relative" }}>
//             <div style={{
//                 width: "100%", 
//                 height: "0", 
//                 padding: "56.25% 0 0 0", 
//                 backgroundImage: `url(${category.image})`,
//                 backgroundPosition: "center",
//                 backgroundSize: "cover"
//             }}></div>
//         </Box>
//         <Heading level="4">{category.title}</Heading>
//     </ Box>
// }