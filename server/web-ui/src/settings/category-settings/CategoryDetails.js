import React from 'react';
import { Box, Form, TextInput, Heading, Button, Layer, TextArea, Text } from 'grommet';
import { Add, Close } from 'grommet-icons';
import { compose, withHandlers, withState } from 'recompose';
import ImagePicker from './ImagePicker';
import CategoryImage from './CategoryImage';

const CategoryNameInput = ({ name, setName, saveChanges }) => {
  return <Box width="60%">
    <TextInput
      placeholder="Category"
      size="xlarge"
      value={name}
      onChange={(event) => setName(event.target.value)}
      onBlur={saveChanges}
    />
  </Box>
}

const baseCategoryDetails = ({ category, setName, saveChanges, selectedCategory, updateCategory, show, setShow, onKeywordsChange }) => {
  return category ? 
  <Box margin="medium" flex="grow">
    <Box flex="grow" align="center">
        <Box width="60%" justify="between" align="center" direction="row">
          <Box width="25%" onClick={() => setShow(true)}>
          { 
            category.image ? 
            <CategoryImage category={category} backgroundUrl={category.image}/> : 
            <Button icon={<Add size="medium" color="red"/> } label="Add Image" color="red"/>
          }
          </Box>
          <CategoryNameInput name={category.name} setName={setName} saveChanges={saveChanges} />
          {show && (
            <Layer
              style={{
                height: "75%",
                width: "50%"
              }}
              onEsc={() => { setShow(false); saveChanges(); }}
              onClickOutside={() => { setShow(false); saveChanges(); }}
            >
              <Box flex={true} margin="medium" align="center">
                  <ImagePicker category={category} updateCategory={updateCategory}></ImagePicker>
                  <Button color="red" label="Close" icon={<Close size="medium" color="red" />} onClick={() => {setShow(false); saveChanges(); }} />
              </Box>
            </Layer>
          )}
        </Box>
        <Box width="100%" flex="grow">
          <Heading margin="xsmall" level="3">Keywords</Heading>
          <Text margin="xsmall">These keywords are what Picto-Search will search on YouTube to look for videos.</Text>
          <Text margin="xsmall">Separate multiple search terms by pressing "Enter".</Text>
          <Box width="100%" flex="grow">
            <TextArea placeholder="keywords" value={category.keywords.join('\n')} onChange={(event) => onKeywordsChange(event.target.value)} resize={false} fill onBlur={saveChanges}/>
          </Box>
        </Box>
      </Box>
    </Box> :
  <Box width="100%" margin="small">
    <Heading level="3">Select or add a category</Heading>
  </Box>
}

const CategoryDetails = compose(
    withState("show", "setShow", false),
    withHandlers({
      setName: ({ category, updateCategory }) => (name) => updateCategory({ ...category, name}),
      setKeywords: ({ category, updateCategory }) => (keywords) => updateCategory({ ...category, keywords }),
      saveChanges: ({ category, categories, selectedCategory, updateProfile }) => () => {
        const newCategories = categories.slice();
        newCategories[selectedCategory] = category;
        updateProfile({ categories: newCategories });
      },
    }),
    withHandlers({
      onKeywordsChange: ({ setKeywords }) => (keywordsText) => {
        const keywords = keywordsText.split('\n');
        setKeywords(keywords);
      }
    })
)(baseCategoryDetails);

export default CategoryDetails;