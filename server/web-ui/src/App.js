import React from 'react';
import { withState, withHandlers, withProps, lifecycle, compose } from 'recompose';
import { Grommet } from 'grommet';
import VideoPlayback from './playback/VideoPlayback';
import CategoryPlayback from './playback/CategoryPlayback';
import Profiles from './profiles/Container';
import ProfileHome from './profile-home/ProfileHome';
import Login from './auth/Login';
import Settings from './settings/Settings';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import Auth from './auth/Auth';
import Loading from './auth/Loading';
import history from './auth/History';
import axios from 'axios';

const theme = {
  global: {
    font: {
      family: 'Roboto',
      size: '14px',
      height: '20px',
    },
    colors: {
      border: {
        dark: "#000",
        light: "#f00"
      },
      brand: "#f00"
    }
  },
};

const PrivateRoute = ({ auth, render, ...rest}) => 
<Route
  {...rest}
  render={(props) => {
      if (!auth.isAuthenticated) console.log("Not authenticated!");
      return auth.isAuthenticated() ?
      render(props) :
      <Redirect
        to={{
          pathname: "/login",
          state: { from: props.location }
        }}
      />
    }
  }
/>

const getProfileFromAccount = (account, name) => account.profiles.find((profile) => profile.name === name);
const getPlaylistFromProfile = (profile, playlistName) => profile.playlists.find((playlist) => playlist.name === playlistName);

const Callback = compose(
  withHandlers({
    handleAuthentication: ({ auth, location }) => (cb) => { if (location.hash) auth.handleAuthentication(cb); }
  }),
  lifecycle({
    componentDidMount() {
      const cb = ({ accessToken }) => {
        const headers = { headers: { 'Authorization': 'Bearer ' + accessToken }};
        axios.get('/api/accounts/authorize', headers)
        .then((response) => {
          this.props.setAccount(response.data);
          history.push("/home");
        })
        .catch((error) => {
          if (error.response.status === 404) {
            axios.post('/api/accounts', { profiles: [] }, headers)
            .then((response) => {
              this.props.setAccount(response.data);
              history.push("/home");
            });
          } else {
            console.log(error);
          }
        });
      }

      this.props.handleAuthentication(cb);
    }
  })
)(Loading);

const baseApp = ({ account, setAccount, updateAccount, auth }) => {
  console.log(account);
  return <Grommet full theme={theme} className="grommet">
    <Router history={history}>
      <Switch>
        <PrivateRoute 
          exact
          path="/profile/:name/category/:categoryName"
          auth={auth}
          render={({ match }) => <CategoryPlayback 
            profile={getProfileFromAccount(account, match.params.name)}
            categoryName={match.params.categoryName} account={account} updateAccount={updateAccount}
          />}
        />
        <PrivateRoute
          exact
          path="/profile/:name/playlist/:playlistName"
          auth={auth}
          render={({ match }) => <VideoPlayback
            profile={getProfileFromAccount(account, match.params.name)}
            videos={getPlaylistFromProfile(getProfileFromAccount(account, match.params.name), match.params.playlistName).videos}
            account={account} updateAccount={updateAccount}
          /> }
        />
        <PrivateRoute 
          exact
          path="/profile/:name"
          auth={auth}
          render={({ match }) => <ProfileHome profile={getProfileFromAccount(account, match.params.name)} />}
        />
        <PrivateRoute 
          exact 
          path="/settings" 
          auth={auth}
          render={() => <Settings account={account} updateAccount={updateAccount}/>}
        />
        <PrivateRoute 
          exact 
          path="/home"
          auth={auth}
          render={() => <Profiles auth={auth} profiles={account.profiles}/>}
        />
        <Route
          path="/loading"
          render={() => <Loading/>}
        />
        <Route
          path="/login"
          render={() => localStorage.getItem('isLoggedIn') ? <Redirect to={"/home"} /> : <Login auth={auth}/>}
        />
        <Route
          path="/callback"
          render={(props) => <Callback setAccount={setAccount} auth={auth} { ...props} />}
        />
        <Redirect to={"/home"}/>
      </Switch>
    </Router>
  </Grommet>
}

const authService = new Auth();

const App = compose(
  withProps({ auth: authService }),
  withState("account", "setAccount", null),
  lifecycle({
    componentDidMount() {
      if (localStorage.getItem('isLoggedIn') === 'true') {
        const cb = ({ accessToken }) => {
          const headers = { headers: { "Authorization": "Bearer " + accessToken } }
          axios.get('/api/accounts/authorize', headers)
          .then((response) => {
            this.props.setAccount(response.data);
            history.replace('/home');
          });
        }
        this.props.auth.renewSession(cb);
        history.push("/loading");
      }
    }
  }),
  withHandlers({
    updateAccount: ({ account, setAccount, auth }) => ( newData ) => {
      const newAccount = { ...account, ...newData };
      axios.put('/api/accounts', newAccount, { headers: { 'Authorization': 'Bearer ' + auth.getAccessToken() } });
      setAccount( newAccount );
    }
  })
)(baseApp);

export default App;
