import yellow from './yellow.jpg';
import green from './green.jpg';
import black from './black.jpg';
import pink from './pink.jpg';
import orange from './orange.jpg';
import blue from './blue.jpg';
import turquoise from './turquoise.jpg';
import purple from './purple.jpg';
import cyan from './cyan.jpg';

export default [
  yellow,
  green,
  black,
  pink,
  orange,
  blue,
  turquoise,
  purple,
  cyan
]