import React from 'react';
import { compose, lifecycle, withState } from 'recompose';
import YouTube from 'react-youtube';
import { Box } from 'grommet';
import './Video.css';

const baseVideo = ({fullscreen, setFullscreen, onReady, onPlay, onPause, onEnd, onError, onStateChange, videoId}) => {
  const opts = {
    height: 1080,
    width: 1920,
    playerVars: {
      autoplay: 1,
      modestbranding: 1,
      controls: 0,
      enablejsapi: 1,
      disablekb: 1,
      fs: 0
    }
  }

  
  return <Box className={ fullscreen ? "playback-video fullscreen" : "playback-video" }>
    <Box className="video-inner">
      <YouTube
        className="video"
        videoId={videoId}
        opts={opts}
        onReady={onReady}
        onPlay={onPlay}
        onPause={onPause}
        onEnd={onEnd}
        onError={onError}
        onStateChange={onStateChange}
      />
    </Box>
    <Box className="video-click" onClick={() => setFullscreen(!fullscreen)}/>
  </Box>
}

const Video = compose(
  withState("fullscreen", "setFullscreen", true),
  lifecycle({
    componentDidMount() {
      const setPlayerOnReady = (event) => { 
        event.target.playVideo(); 
        this.props.setFullscreen(true); 
        return this.props.setPlayer(event.target) 
      };
      this.props.eventHandler.addReadyEvents({ setPlayerOnReady });
    }
  })
)(baseVideo);

export default Video;