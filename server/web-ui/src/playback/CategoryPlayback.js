import VideoPlayback from './VideoPlayback';
import Loading from '../auth/Loading';
import { withState, withProps, renderComponent, lifecycle, branch, compose } from 'recompose';
import axios from 'axios';
import _ from 'lodash';

const CategoryPlayback = compose(
  withProps(({ profile, categoryName }) => ({ category: profile.categories.find((category) => category.name === categoryName) })),
  withState("requestCount", "setRequestCount", ({ category }) => category.keywords.length ),
  withState("resultsSet", "setResultsSet", []),
  lifecycle({
    componentDidMount() {
      this.props.category.keywords.forEach((keyword) => axios.get(`/api/yt?query=${keyword}`)
        .then((response) => {
          console.log(response);
          this.props.setResultsSet([ ...this.props.resultsSet, ...response.data.items ]);
          this.props.setRequestCount(this.props.requestCount - 1);
        })
      );
    }
  }),
  branch(({ requestCount }) => requestCount !== 0, renderComponent(Loading)),
  withProps(({ resultsSet }) => {
    const resultsSubset = _.shuffle(_.uniq(resultsSet, (a, b) => a.id.videoId === b.id.videoId)).slice(0, 50);
    console.log(resultsSubset);
    const videos = resultsSubset.map((searchResult) => ({
      videoId: searchResult.id.videoId,
      title: searchResult.snippet.title
    }));
    return { videos };
  })
)(VideoPlayback);

export default CategoryPlayback;