import React from 'react';
import { Box } from 'grommet';
import './PlayerControl.css';

const PlayerControl = ({ children, ...props }) =>
<Box className="player-control" { ...props } width="160px" height="160px">
  {children}
</Box>

export default PlayerControl;