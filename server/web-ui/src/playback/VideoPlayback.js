import React from 'react';
import { withState, withProps, withHandlers, compose, withReducer } from 'recompose';
import { Box } from 'grommet';
import Menu from './Menu';
import Video from './Video';
import Player from './Player';
import './VideoPlayback.css';

//const videoIds = profile.playlists.find((playlist) => playlist.name === playlistTitle).videos;

const baseVideoPlayback = ({
  player, setPlayer, videos, index, updateIndex, profile, account, updateAccount, 
  onEnd, onPlay, onPause, onError, onReady, onStateChange, eventHandler
}) =>
{
  return <Box className="playback-container-outer" fill>
    <Box className="playback-container" direction="row" flex="grow">
      <Video
        videoId={videos[0].videoId}
        setPlayer={setPlayer}
        onEnd={onEnd}
        onPlay={onPlay}
        onPause={onPause}
        onError={onError}
        onReady={onReady}
        onStateChange={onStateChange}
        eventHandler={eventHandler}
      />
      <Player player={player} profileName={profile.name} videos={videos} index={index} updateIndex={updateIndex}
        eventHandler={eventHandler}
        profile={profile} account={account} updateAccount={updateAccount}
      />
      <Menu player={player} videos={videos} index={index} updateIndex={updateIndex} eventHandler={eventHandler}/>
    </Box>
  </Box>
}

const eventReducer = (events, newEvents) => Object.assign(events, newEvents);
const eventInitialState = {
  endEvents: {},
  playEvents: {},
  pauseEvents: {},
  errorEvents: {},
  readyEvents: {},
  stateChangeEvents: {}
}

const VideoPlayback = compose(
  withState("player", "setPlayer", null),
  withState("index", "setIndex", 0),
  withReducer("events", "updateEvents", eventReducer, eventInitialState),
  withProps(({events, updateEvents}) => ({
    eventHandler: {
      addEndEvents: (newEvents) => updateEvents({ endEvents: { ...events.endEvents, ...newEvents }}),
      addPlayEvents: (newEvents) => updateEvents({ playEvents: { ...events.playEvents, ...newEvents }}),
      addPauseEvents: (newEvents) => updateEvents({ pauseEvents: { ...events.pauseEvents, ...newEvents }}),
      addErrorEvents: (newEvents) => updateEvents({ errorEvents: { ...events.errorEvents, ...newEvents }}),
      addReadyEvents: (newEvents) => updateEvents({ readyEvents: { ...events.readyEvents, ...newEvents }}),
      addStateChangeEvents: (newEvents) => updateEvents({ stateChangeEvents: { ...events.stateChangeEvents, ...newEvents }}),
    }
  })),
  withHandlers({
    onEnd: ({ events }) => (event) => Object.values(events.endEvents).forEach((fn) => fn(event)),
    onPlay: ({ events }) => (event) => Object.values(events.playEvents).forEach((fn) => fn(event)),
    onPause: ({ events }) => (event) => Object.values(events.pauseEvents).forEach((fn) => fn(event)),
    onError: ({ events }) => (event) => Object.values(events.errorEvents).forEach((fn) => fn(event)),
    onReady: ({ events }) => (event) => Object.values(events.readyEvents).forEach((fn) => fn(event)),
    onStateChange: ({ events }) => (event) => Object.values(events.stateChangeEvents).forEach((fn) => fn(event)),
  }),
  withHandlers({
    updateIndex: ({ videos, setIndex }) => (newIndex) => setIndex(Math.min(videos.length, newIndex))
  })
)(baseVideoPlayback);

export default VideoPlayback;