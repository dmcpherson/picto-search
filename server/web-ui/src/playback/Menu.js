import React from 'react';
import { Box } from 'grommet';
import MenuEntry from './MenuEntry';
import './Menu.css';

const Menu = ({ player, videos, index, updateIndex }) => {
  

  const generateMenuEntries = () => {
    var nextIndex = index + 1;
    if (videos.length < 4) {
      var entries = [];
      for (var i = 0; i < 4; i++) {
          if (nextIndex === videos.length) nextIndex = 0;
          entries = [...entries, <MenuEntry player={player} updateIndex={updateIndex} video={videos[nextIndex]} index={nextIndex} key={i}/>];
          nextIndex++;
      }
      return entries;
    } else if (nextIndex === videos.length) {
      const segment = videos.slice(0, 4);
      return segment.map((video, key) => <MenuEntry player={player} updateIndex={updateIndex} video={video} index={(nextIndex + key) % videos.length} key={key}/>);
    } else if (nextIndex > videos.length - 4) {
      const endSegment = videos.slice(nextIndex);
      const startSegment = videos.slice(0, 4 - endSegment.length);
      const segment = [...endSegment, ...startSegment];
      return segment.map((video, key) => <MenuEntry player={player} updateIndex={updateIndex} video={video} index={(nextIndex + key) % videos.length} key={key}/>);
    } else {
      const segment = videos.slice(nextIndex, nextIndex + 4);
      return segment.map((video, key) => <MenuEntry player={player} updateIndex={updateIndex} video={video} index={(nextIndex + key) % videos.length} key={key}/>);
    }
  }
  return <Box
    className="playback-menu"
    direction="column"
    justify="between"
    flex
  >
    { generateMenuEntries() }
  </Box>
}

export default Menu;