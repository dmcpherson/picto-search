import React, { useState } from 'react';
import { Box } from 'grommet';
import './MenuEntry.css';

const MenuEntry = ({ video, player, index, updateIndex }) => {
  return <Box className="menu-entry" direction="row" align="center" onClick={()=>{
      updateIndex(index);
      player.loadVideoById(video.videoId);
  }}>
    <Box className="menu-entry-thumbnail" width="320px" height="180px">
      <img src={`https://img.youtube.com/vi/${video.videoId}/mqdefault.jpg`} alt={`Video #${index}`}/>
    </Box>
    <h2>{video.title}</h2>
  </Box>
}

export default MenuEntry;