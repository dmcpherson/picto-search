import React from 'react';
import { Box } from 'grommet';
import { lifecycle, compose, withState } from 'recompose';
import PlayerControl from './PlayerControl';
import { Play, Pause, Close, Favorite, RotateLeft, FastForward } from 'grommet-icons';
import history from './../auth/History';
import './PlayerControls.css';

const basePlayerControls = ({ player, videos, index, updateIndex, profileName, profile, account, updateAccount, playing, setPlaying }) => {
  console.log(videos);
  console.log(index);
  let favorite = false;
  if (profile.favoritePlaylist !== undefined) {
    profile.favoritePlaylist.videos.forEach(vid => {
      if (vid.videoId === videos[index].videoId) favorite = true;
    });
  }
  return <Box
    className="controls"
    justify="around"
    margin="medium"
    direction="row"
    align="end"
    flex
    >
      <PlayerControl style={{ backgroundColor: favorite ? "#ff287e" : "#ffb7ed" }}>
      <Favorite color={"#ff70db"} style={{ marginTop: "23%"}} size='xlarge'/>
        <Box
          className="clickable"
          onClick={() => {
            const updatedAccount = Object.assign({}, account);
            let profileIndex = -1;
            updatedAccount.profiles.forEach((prof, i) => {
              if (profileName === prof.name) {
                profileIndex = i;
              }
            });
            if (profileIndex !== -1) {
              if (updatedAccount.profiles[profileIndex].favoritePlaylist === undefined) {
                updatedAccount.profiles[profileIndex].favoritePlaylist = {
                  name: 'Favorites',
                  videos: [],
                };
              }
              let existsIndex = -1;
              updatedAccount.profiles[profileIndex].favoritePlaylist.videos.forEach((video, i) => {
                if (video.videoId === videos[index].videoId) {
                  existsIndex = i;
                }
              });
              if (existsIndex !== -1) {
                updatedAccount.profiles[profileIndex].favoritePlaylist.videos.splice(existsIndex, 1);
              } else {
                updatedAccount.profiles[profileIndex].favoritePlaylist.videos.push({
                  videoId: videos[index].videoId
                });
              }

              updateAccount(updatedAccount);
            }
          }}
        />
      </PlayerControl>
      <PlayerControl alignSelf="start">
        <RotateLeft color={"#55a0cc"} size='xlarge'/>
        <Box
          className="clickable"
          onClick={() => { player.seekTo(0, true) }}
        />
      </PlayerControl>
      <PlayerControl>
        { playing ? <Pause color={"#55a0cc"} size='xlarge'/> : <Play color={"#55a0cc"} style={{ marginLeft: "24%"}} size='xlarge'/> }
        <Box
          className="clickable"
          onClick={() => playing ? player.pauseVideo() : player.playVideo()}
        />
      </PlayerControl>
      <PlayerControl alignSelf="start">
        <FastForward color={"#55a0cc"} style={{ marginLeft: "26%" }} size='xlarge'/>
        <Box
          className="clickable"
          onClick={() => {
            const nextIndex = index + 1 >= videos.length ? 0 : index + 1;
            updateIndex(nextIndex);
            player.loadVideoById(videos[nextIndex]);
          }}
        />
      </PlayerControl>
      <PlayerControl style={{ backgroundColor: "#faa" }}>
        <Close color={"#ce3737"} size='xlarge'/>
        <Box
          className="clickable"
          onClick={() => history.push(`/profile/${profileName}`)}
        />
      </PlayerControl>
  </Box>
}

const PlayerControls = compose(
  withState("playing", "setPlaying", true),
  lifecycle({
    componentDidMount() {
      this.props.eventHandler.addPlayEvents({ setPlayingTrue: () => this.props.setPlaying(true) });
      this.props.eventHandler.addPauseEvents({ setPlayingFalse: () => this.props.setPlaying(false) });
    }
  })
)(basePlayerControls);

export default PlayerControls;