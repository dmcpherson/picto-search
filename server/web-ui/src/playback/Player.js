import React from 'react';
import { lifecycle } from 'recompose';
import { Box } from 'grommet';
import PlayerControls from './PlayerControls';
import DummyVideo from './DummyVideo';
import './Player.css';

const basePlayer = ({ player, videos, index, updateIndex, profile, eventHandler, profileName, account, updateAccount }) => {
  return <Box 
    className="player-container-outer"
    direction="column"
  >
    <DummyVideo/>
    <PlayerControls player={player} profileName={profileName} videos={videos} index={index}
      updateIndex={updateIndex} eventHandler={eventHandler}
      profile={profile} account={account} updateAccount={updateAccount}
    />
  </Box>
}

const Player = lifecycle({
  componentDidMount() {
    const loadNextVideo = () => {
      const newIndex = this.props.updateIndex(this.props.index + 1);
      this.props.player.loadVideoById(this.props.videos[newIndex]);
    }
    this.props.eventHandler.addEndEvents({ loadNextVideo });
  }
})(basePlayer);

export default Player;