import React from 'react';
import { Box } from 'grommet';
import './DummyVideo.css';

const DummyVideo = () =>
<Box className="dummy-video"/>

export default DummyVideo;