import React from 'react';
import {Box} from 'grommet';
import { CircleSpinner } from 'react-spinners-kit';

const Loading = () =>
<Box fill>
  <Box margin="auto">
  <CircleSpinner
    size={300}
    color="#f00"
    loading={true}
  />
  </Box>
</Box>

export default Loading;