import React from 'react';
import { Box, Button, Heading } from 'grommet';

const Login = ({ auth }) =>
<Box fill>
  <Box margin="auto">
    <Heading>Welcome to Picto-Search!</Heading>
    <Button
      label="Click here to log in"
      color="red"
      onClick={()=>{ auth.login(); }}
    />
  </Box>
</Box>

export default Login;