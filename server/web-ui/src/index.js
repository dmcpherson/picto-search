import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
const phAccount = {
  profiles: [
    {
      name: "Matt",
      avatar: 0,
      categories: [],
      playlists: [],
      favoritePlaylist: {
        name: "Favorites",
        videos: []
      }
    },
    {
      name: "Gerhard",
      avatar: 0,
      categories: [],
      playlists: [
        {
          name: "TestG",
          videos: [
          "rk5XuqLrf3U",
          "tGWbNgaR6qk"
          ]
        }
      ],
      favoritePlaylist: {
        name: "Favorites",
        videos: []
      }
    },
    {
      name: "Devin",
      avatar: 1,
      categories: [
        {
          name: "Test",
          image: "https://images.pexels.com/photos/850602/pexels-photo-850602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=125&w=125",
          safeSearch: "strict",
          keywords: [
            "Dog",
            "Dogs",
            "Puppies"
          ]
        },
        {
          name: "Test2",
          image: "https://images.pexels.com/photos/850602/pexels-photo-850602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=125&w=125",
          safeSearch: "strict",
          keywords: [
            "Dog",
            "Dogs",
            "Puppies"
          ]
        },
        {
          name: "Test3",
          image: "https://images.pexels.com/photos/850602/pexels-photo-850602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=125&w=125",
          safeSearch: "strict",
          keywords: [
            "Dog",
            "Dogs",
            "Puppies"
          ]
        },
        {
          name: "Test4",
          image: "https://images.pexels.com/photos/850602/pexels-photo-850602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=125&w=125",
          safeSearch: "strict",
          keywords: [
            "Dog",
            "Dogs",
            "Puppies"
          ]
        },
        {
          name: "Test5",
          image: "https://images.pexels.com/photos/850602/pexels-photo-850602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=125&w=125",
          safeSearch: "strict",
          keywords: [
            "Dog",
            "Dogs",
            "Puppies"
          ]
        },
      ],
      playlists: [
        {
          name: "Favorites",
          videos: [
            "rAt1vxwQG9c",
            "mRf3-JkwqfU",
            "rk5XuqLrf3U",
            "hY7m5jjJ9mM"
          ]
        },
        {
          name: "Playlist 2",
          videos: [
            "rAt1vxwQG9c",
            "mRf3-JkwqfU",
            "rk5XuqLrf3U",
            "hY7m5jjJ9mM"
          ]
        },
        {
          name: "Playlist 3",
          videos: [
            "rAt1vxwQG9c",
            "mRf3-JkwqfU",
            "rk5XuqLrf3U",
            "hY7m5jjJ9mM"
          ]
        }
      ],
      favoritePlaylist: {
        name: "Favorites",
        videos: []
      }
    },
    {
      name: "Kyle",
      avatar: 2,
      categories: [
        {
          name: "",
          image: "",
          safeSearch: "strict",
          keywords: []
        }
      ],
      playlists: [],
      favoritePlaylist: {
        name: "Favorites",
        videos: []
      }
    },
  ]
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
