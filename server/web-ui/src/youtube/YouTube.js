import axios from 'axios';

const YouTubeService = {
  getVideoDetails: (id) => {
    return axios.get(`/api/yt/${id}`);
  }
}

export default YouTubeService;